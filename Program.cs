﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace Test_Selenium
{
    class Program
    {
        static void Main(string[] args)
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.PageLoadStrategy = PageLoadStrategy.Eager;
            IWebDriver driver = new ChromeDriver("C:\\Users\\Vizi\\Downloads\\chromedriver_win32", chromeOptions);
            openBroswer(driver);
            enterName(driver);
            selectGender(driver);
            selectExp(driver);
            enterDate(driver);
            selectProfession(driver);
            selectTools(driver);
            selectContinent(driver);
            selectCommands(driver);
            clickSubmit(driver);

        }

        static void openBroswer(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://www.techlistic.com/p/selenium-practice-form.html");
            driver.Manage().Window.Maximize();
            IWebElement cookies = driver.FindElement(By.Id("cookieChoiceDismiss"));
            cookies.Click();
        }

        static void enterName(IWebDriver driver)
        {
            IWebElement firstName = driver.FindElement(By.Name("firstname"));
            firstName.SendKeys("Alexandru");
            IWebElement lastName = driver.FindElement(By.Name("lastname"));
            lastName.SendKeys("Vizitiu");
        }

        static void selectGender(IWebDriver driver)
        {
            IWebElement male = driver.FindElement(By.Id("sex-0"));
            male.Click();
        }

        static void selectExp(IWebDriver driver)
        {
            IWebElement experience = driver.FindElement(By.Id("exp-0"));
            experience.Click();
        }

        static void enterDate(IWebDriver driver)
        {
            IWebElement date = driver.FindElement(By.Id("datepicker"));
            date.SendKeys("30.07.2021");
        }

        static void selectProfession(IWebDriver driver)
        {
            IWebElement profession = driver.FindElement(By.Id("profession-0"));
            profession.Click();
        }

        static void selectTools(IWebDriver driver)
        {
            IWebElement tool0 = driver.FindElement(By.Id("tool-0"));
            IWebElement tool2 = driver.FindElement(By.Id("tool-2"));
            tool0.Click();
            tool2.Click();
        }

        static void selectContinent(IWebDriver driver)
        {
            IWebElement continents = driver.FindElement(By.Id("continents"));
            SelectElement continentChoice = new SelectElement(continents);
            continentChoice.SelectByIndex(1);
        }

        static void selectCommands(IWebDriver driver)
        {
            IWebElement commands = driver.FindElement(By.Id("selenium_commands"));
            SelectElement commandsChoice = new SelectElement(commands);
            commandsChoice.SelectByIndex(0);
            commandsChoice.SelectByIndex(1);
            commandsChoice.SelectByIndex(4);
        }

        static void clickSubmit(IWebDriver driver)
        {
            IWebElement submitButton = driver.FindElement(By.Id("submit"));
            submitButton.Click();
        }
    }
}
